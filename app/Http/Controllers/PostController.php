<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            Post::all()->where('deleted', 0)
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::create($request->all());
        return response()->json($post, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if(is_null($id)){

            return response()->json(
                [''=>'Que no hay nada'], 
                404
            );
        
        } else {
            return response()->json($post);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::where('id', $id)->first()->get();
        if(is_null($post)){
            return response()->json(['msg'=>'not Found'], 404);
        }else{
            Post::find($id)->update(['message'=>$request->message]);
            return response()->json(['msg'=>'modificado'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $count = Post::where('id', $id)->count();

        if($count == 1){
            Post::find($id)->update(['deleted'=>1]);
            return response()->json(['msg'=>"eliminado"], 200);
        }else{
            return response()->json(['msg'=>"not found"], 404);
        }
    }
}
