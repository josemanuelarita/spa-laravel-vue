import Vue from 'vue';
import VueRouter from 'vue-router';

import PagePost from './pages/Post';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/posts',
            component: PagePost,
        },
    ]
});

export default router;
